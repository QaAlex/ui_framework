import allure
from selenium.webdriver.common.by import By
from pages.main_page import MainPage
from pages.base_page import BasePage
from conftest import driver


class RegistrationPage(BasePage):
    path = "/register"

    def registration_form_title(self):
        return self.element(("Тайтл у формы регистрации","//h2[text()='Регистрация']"))

    def name_field(self):
        return self.element(("Поле ввода имени на странице регистарции","(//input[@name='name'])[1]"))  #//label[text()='Имя']/following-sibling::input  не нашел по такому локатору

    def email_field(self):
        return self.element(("Поле ввода email на странице регистарции","(//input[@name='name'])[2]"))

    def password_field(self):
        return self.element(("Поле ввода пароля на странице регистарции","//input[@type='password']"))

    def registration_btn(self):
        return self.element(("Кнопка зарегистрироваться","//button[text()='Зарегистрироваться']"))

    def open_password(self):
        return self.element(("Глазик в поле ввода пароля","div[@class='input__icon input__icon-action']"))

    @allure.step("Проверка что находимся на странице 'регистрации'")
    def should_be_on_registration_page(self):
        assert self.registration_form_title().text == 'Регистрация', f'Wrong page'
        assert 'register' in self._driver.current_url, f'Wrong page'

    @allure.step("Заполнить форму регистрации")
    def fill_registration_form(self, name: str, email: str, password: str, is_valid=True):
        from pages.login_page import LoginPage
        self.name_field().fill(name)
        self.email_field().fill(email)
        self.password_field().fill(password)
        self.registration_btn().click()
        if is_valid:
            return LoginPage(self._driver)
        else:
            return self

    @allure.step("Получить сообщение такой email уже существует")
    def get_message_user_already_exists(self):
        message = self.error_message().text
        return message

    @allure.step("Получить сообщение некорректный пароль")
    def get_message_wrong_password(self):
        assert self.error_message().text == 'Некорректный пароль'
