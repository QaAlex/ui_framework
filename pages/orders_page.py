import allure

from pages.base_page import BasePage


class OrdersPage(BasePage):
    path = '/feed'

    def order_list(self):
        return self.elements(("Список заказов", "//ul[@class='OrderFeed_orderList__cBvyi']/li"))

    def order_block(self):
        return self.element(("Блок с карточками заказов", "//ul[@class='OrderFeed_list__OLh59']"))

    def order_card(self):
        return self.order_block().inner_element(("Карточка заказа",
                                                 "//li[contains(@class , 'OrderHistory_listItem')]"))

    def numbers_of_orders(self):
        return self.elements(("Номера заказов","//p[@class ='text text_type_digits-default']"))

    @allure.step ("Открыть модалку с заказом")
    def click_on_order_card(self):
        from pages.ordermodal import OrderModal
        self.order_card().click()
        return OrderModal(self.browser)


