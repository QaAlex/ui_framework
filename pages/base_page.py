import allure
from selenium.webdriver.chrome.webdriver import ChromiumDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from helpers.browser import Browser


class BasePage(Browser):
    BASE_URL = 'https://stellarburgers.nomoreparties.site'

    def __init__(self, driver):
        super().__init__(driver)
        self._driver: ChromiumDriver = driver
        self._wait = WebDriverWait(self._driver, timeout=10)
        self._header_block = None

    @property
    def path(self):
        raise NotImplementedError

    @property
    def header_block(self):
        from pages.header_block import HeaderBlock
        if self._header_block is None:
            self._header_block = HeaderBlock(self._driver)
        return self._header_block

    def open(self):
        self._driver.get(self.BASE_URL + self.path)

    def find_element_(self, by: str, locator: str) -> WebElement:
        self.until = self._wait.until(EC.presence_of_element_located((by, locator)))
        return self.until

    def find_elements_(self, by: str, locator: str) -> WebElement:
        return self._wait.until(EC.presence_of_all_elements_located((by, locator)))

    @allure.step("Получение сообщения об ошибке")
    def error_message(self):
        return self.element(("Сообщение об ошибке", "//p[@class = 'input__error text_type_main-default']"))



