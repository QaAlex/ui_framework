import allure
from selenium.webdriver.common.by import By
from pages.login_page import LoginPage
from pages.personal_account_page import PersonalAccountPage
from pages.base_page import BasePage


class HeaderBlock(BasePage):

    def login_link(self):
        return self.element(("Ссылка логин в хедере", "//p[text()='Личный Кабинет']/.."))

    def header_logo(self):
        return self.element(("Логотип конструктор в хедере", "//a[contains(@class, 'AppHeader_header__link_active')]"))

    def orders_link(self):
        return self.element(("Ссылка лента заказов", "//li[@class='undefined ml-2']/a"))

    def burger_logo(self):
        return self.element(("Логотип бургер", ".AppHeader_header__logo__2D0X2"))

    @allure.step("Кликнуть по ссылке логин в хедере.")
    def click_on_login_link(self, is_resister: bool = False):
        self.login_link().click()
        if is_resister:
            return PersonalAccountPage(self._driver)
        return LoginPage(self._driver)

