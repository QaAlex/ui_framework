import allure
from selenium.webdriver.common.by import By
from pages.main_page import MainPage
from pages.base_page import BasePage
from pages.registration_page import RegistrationPage


class LoginPage(BasePage):
    path = "/login"

    def login_form_title(self):
        return self.element(("Тайтл у формы логина", "//h2[text()='Вход']"))

    def email_field(self):
        return self.element(("Поле email", "//input[@name='name']"))

    def password_field(self):
        return self.element(("Поле пароль", "//input[@type='password'] |"
                                            " (//div[contains(@class, 'input_size_default')]/input)[2]"))

    def login_btn(self):
        return self.element(("Кнопка войти", "//button[text()='Войти']"))

    def registration_link(self):
        return self.element(("Ссылка регистрации", '//a[contains(@class, "Auth_link")]'))

    def show_password_btn(self):
        return self.element(("Кнопка открыть пароль", "//div[@class='input__icon input__icon-action']"))

    @allure.step("Проверка что перешли на страницу логина.")
    def should_be_on_login_page(self):
        assert self.login_form_title().text == 'Вход', f'Wrong page'
        assert 'login' in self._driver.current_url, f'Wrong page'

    @allure.step("Заполнить форму логина")
    def fill_login_form(self, email: str, password: str, is_valid=True):
        self.email_field().fill(email)
        self.password_field().fill(password)
        self.login_btn().click()
        if is_valid:
            return MainPage(self._driver)
        return self

    @allure.step("Нажать на кнопку зарегистрироваться.")
    def click_on_registration_link(self):
        self.registration_link().click()
        return RegistrationPage(self._driver)

    @allure.step("Нажать на кнопку показать пароль.")
    def click_on_show_password(self):
        self.password_field().should_have_attribute('type', 'password')
        self.show_password_btn().click()
        self.password_field().should_have_attribute('type', 'text')
        return self

