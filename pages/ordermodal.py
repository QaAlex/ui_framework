import allure

from pages.orders_page import OrdersPage
from pages.base_page import BasePage


class OrderModal(BasePage):

    def order_modals(self):
        return self.element(("Модалка с заказом", "//div[contains(@class, 'Modal_orderBox__1xWdi')]/.."))

    def close_modal_btn(self):
        return self.element(("Кнопка закртыия модалки", "//div[contains(@class, 'Modal_orderBox__1xWdi')]/.."
                                                       "//p[ contains (@class,'text text_type_main-medium')]"))

    def number_of_order(self):
        return self.element(("Номер заказа в модалке", "//div[contains(@class, 'Modal_orderBox__1xWdi')]"
                                                       "/..//p[contains(@class, 'text_type_digits-default mb-10')]"))

    @allure.step("Проверка наличия крестика в модалке")
    def should_be_on_order_modals(self):
        self.close_modal_btn().should_be_visible()


    def order_number_compare(self):
        first_order = self.numbers_of_orders()[1]
        assert self.number_of_order().text == first_order.text
