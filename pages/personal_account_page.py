import allure
from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from pages.login_page import LoginPage


class PersonalAccountPage(BasePage):
    def left_menu_page(self):
        return self.element(("Левое меню","//nav[@class='Account_nav__Lgali']"))

    def exit_btn(self):
        return self.element(("Кнопка выход","//button[@class ='Account_button__14Yp3 text text_type_main-medium text_color_inactive']"))

    def account_history(self):
        return self.element(("История заказов","//a[@class ='Account_link__2ETsJ text text_type_main-medium text_color_inactive']"))

    def personal_profile(self):
        return self.element(("Ссылка на профиль","//a[@href='/account/profile' and text()='Профиль']"))
    def edit_btn(self):
        return self.element(("Карандашик в поле ввода","div.input__icon"))

    def name_field(self):
        return self.element(("Поле Имя", "//input[@name='Name']"))

    def login_field(self):
        return self.element(("Поле Логин", "//input[@type='text' and(@name='Name')=False]"))

    def password_field(self):
        return self.element(("Поле пароль", "//input[@type='password']"))

    def cansel_btn(self):
        return self.element(("кнопка Отмена", "//button[text()='Отмена']"))

    def save_btn(self):
        return self.element(("кнопка Сохранить", "//button[text()='Сохранить']"))

    @allure.step("Проверка что находимся на 'странице персональный аккаунт'")
    def should_be_personal_account_page(self):
        left_menu_block = self.element(*self.left_menu_page)
        left_menu_block.is_displayed()

    @allure.step("Изменить данные пользователя")
    def change_user_data(self, name: str = None, login: str = None, password: str = None):
        if name is not None:
            self.name_field().fill(name)
        if login is not None:
            self.login_field().fill(login)
        if password is not None:
            self.password_field().fill(password)
        self.save_btn().click()
        return self

    @allure.step("Выйти из ЛК.")
    def exif_personal_account(self):
        self.exit_btn().click()
        return LoginPage