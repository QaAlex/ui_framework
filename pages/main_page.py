from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import ChromiumDriver
from pages.base_page import BasePage
import allure


class MainPage(BasePage):

    path = '/'

    def burger_title(self):
        return self.element(("Бургер тайтл", "//h1"))

    @allure.step('Наличие  "Соберите бургер" на главной странице')
    def should_be_main_page(self):
        self.burger_title().should_have_text("Соберите бургер", "Wrong page")

    def make_order_btn(self):
        return self.element(("Кнопка оформить заказ", "//*[text()='Оформить заказ']"))

    def enter_personal_account_btn(self):
        return self.element(("Кнопка войти в акаунт", "//*[text()='Войти в аккаунт']"))




