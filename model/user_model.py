from dataclasses import dataclass
from typing import Optional


@dataclass # 'помогает работать с моделями
class UserModel:
    name: Optional[str]
    email: str
    password: str
