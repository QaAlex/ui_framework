import os
import shutil
import sys
from datetime import datetime

import allure
import pytest
from selenium import webdriver
from selenium.webdriver import ChromeOptions
from selenium.webdriver import FirefoxOptions
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from data.register_data import UserModel


def pytest_addoption(parser):
    parser.addoption("--browser", action="store", default="chrome")
    parser.addoption("--browser_ver", action="store", default="115.0")
    parser.addoption("--selenoid", action="store_true")
    parser.addoption("--hub", action="store", default="localhost")
    parser.addoption("--resolution", action="store", default="1920x1080")


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep


@pytest.fixture
def config(request):
    browser = request.config.getoption("--browser")
    version = request.config.getoption("--browser_ver")
    hub = request.config.getoption("--hub")
    selenoid = request.config.getoption("--selenoid")

    return {
        "browser": browser,
        "version": version,
        "hub": hub,
        "selenoid": selenoid
    }


@pytest.fixture
def driver(request, config, project_root):
    if config.get("selenoid"):
        option = ChromeOptions()
        option.set_capability("browserName", config.get("browser"))
        option.set_capability("browserVersion", config.get("version"))
        driver = webdriver.Remote("http://127.0.0.1:4444/wd/hub", options=option)

    elif config.get("browser") == "chrome":
        options = ChromeOptions()
        servie = ChromeService(executable_path=ChromeDriverManager().install())
        driver = webdriver.Chrome(options=options, service=servie)
    elif config.get("browser") == "firefox":
        clear_profile()
        options = FirefoxOptions()
        service = FirefoxService(executable_path=GeckoDriverManager().install())
        options.binary = FirefoxBinary('/snap/bin/firefox')
        options.add_argument("--profile=/home/alex/test_profile/")
        driver = webdriver.Firefox(options=options, service=service)
    else:
        raise pytest.UsageError("--browser_name should be chrome or firefox")

    print("Browser was started")
    print("Browser was started")
    yield driver
    if request.node.rep_call.failed:
        screenshot_path = os.path.join(project_root, 'screenshots/failure.JPG')
        driver.save_screenshot(screenshot_path)
        allure.attach.file(screenshot_path, 'failure.png', attachment_type=allure.attachment_type.JPG)
    driver.quit()


def pytest_configure(config):
    if not hasattr(config, "workerinput"):
        sys.stderr.write('\nFIREFOX profile was cleared\n')
        sys.stderr.flush()
        clear_profile()

# @pytest.fixture
# def login(driver):
#     base_suite = TestBaseSuite()
#     # TODO
#     from pages.main_page import MainPage
#     user = UserModel(name=None, email="test123@gmail.com", password="qwerty123")
#     main_page = base_suite.get_page(MainPage)
#     login_page = main_page.header_block.click_on_login_link()
#     login_page.fill_login_form(user.email, user.password)
#     return MainPage(driver)


def clear_profile():
    path = '/home/alex/test_profile/'
    for item in os.listdir(path):
        if os.path.isfile(path + item):
            os.remove(path + item)
        elif os.path.isdir(path + item):
            shutil.rmtree(path + item)


@pytest.fixture(scope='session')
def project_root():
    return os.path.abspath(os.path.join(__file__, os.path.pardir))
