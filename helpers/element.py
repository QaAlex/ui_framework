from typing import Tuple

import allure
from selenium.common import TimeoutException
from selenium.webdriver.common.actions.action_builder import ActionBuilder
from selenium.webdriver.common.actions.mouse_button import MouseButton
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.webdriver import ChromiumDriver
from selenium.webdriver.common.action_chains import ActionChains


class Element:

    def __init__(self, browser: ChromiumDriver, locator: Tuple[str, str]):
        self.__browser = browser
        self.name, self.path = locator
        self.__element = self.find_element_()
        self.action = ActionChains(self.__browser)

    @property
    def clear(self):
        self.__element.clear()
        return self

    @property
    def text(self):
        return self.__element.text

    @property
    def value(self):
        return self.__element.get_attribute('value')

    @property
    def number(self):
        num = round(float(self.__element.text), 2)
        return int(num) if num % 1 == 0.0 else num   # тернарный оператор.  Вернет int , если выполнится условие, или округленный float, если не выполнится

    @property
    def all(self):
        return Elements(self.__browser, (self.name, self.path))

    def find_element_(self) -> WebElement:
        # TODO create timeout message
        return WebDriverWait(self.__browser, timeout=10) \
            .until(EC.visibility_of_element_located((self.search_strategy(), self.path)))

    def execute_script(self, script, *args):
        self.__browser.execute_script(script, *args)
        return self

    def inner_element(self, locator: Tuple[str, str]):
        self.path += locator[1]
        return self.find_element_()

    def click(self):
        self.__element.click()

    def double_click(self):
        self.action.double_click(self.__element).perform()

    def search_strategy(self):
        if self.path.startswith('/') or self.path.startswith('('):
            return By.XPATH
        else:
            return By.CSS_SELECTOR

    def fill(self, value):
        self.__element.clear()
        self.__element.send_keys(str(value))
        return self

    def move_to_element(self):
        self.action.move_to_element(self.__element).perform()

    def scroll_to_element(self):
        self.action.scroll_to_element(self.__element)

    def get_property(self, property_name: str):
        return self.__element.get_property(name=property_name)

    def hover(self):
        self.action.move_to_element(self.__element).perform()

    def hover_with_offset(self, x: int, y: int):
        self.action.move_to_element_with_offset(self.__element, x, y).perform()

    def scroll_into_view(self):
        return self.execute_script("arguments[0].scrollIntoView({block: 'center', behavior: 'auto'});",
                                   self.__element)

    def hide(self):
        self.execute_script("arguments[0].style.display = 'none';", self.__element)

    def click_on_mouse_wheel(self):
        action = ActionBuilder(self.__browser)
        action.pointer_action.click(self.__element, button=MouseButton.MIDDLE)
        action.perform()

    @allure.step("Проверка наличия заданного текста")
    def should_have_text(self, text):
        assert self.__element.text == text
        return self

    @allure.step("Проверка видимости элемента")
    def should_be_visible(self):
        try:
            WebDriverWait(self.__browser, 10).until(
                EC.visibility_of_element_located((self.search_strategy(), self.path)))
        except TimeoutException:
            pass

    def should_have_class(self, clas_name: str):
        assert (self.__element.get_attribute('class') == clas_name,
                f"Element {self.__element} does not have class {clas_name}")

    def should_have_attribute(self, name, value):
        assert self.__element.get_attribute(name) == value, f"Element {self.__element} does not have attribute {value}"

class Elements:
    def __init__(self, browser: ChromiumDriver, locator: Tuple[str, str]):
        self.__browser = browser
        self.name, self.path = locator
        self.__elements: list[Element] = self.find_elements()

    def search_strategy(self):
        if self.path.startswith('/') or self.path.startswith('('):
            return By.XPATH
        else:
            return By.CSS_SELECTOR

    def find_elements(self):
        return WebDriverWait(self.__browser, 10).until(
            EC.presence_of_all_elements_located((self.search_strategy(), self.path))
        )

    def __len__(self):
        return len(self.__elements)

    def __repr__(self):
        return f"\n{self.name}: {self.path}"

    def __iter__(self):
        for i in range(len(self.__elements)):
            yield self.__elements[i]

    @property
    def texts(self):
        return [element.text for element in self.__elements]

    def get_element_by_text(self, text: str) -> Element:
        for element in self.__elements:
            if text == element.text:
                return element
        assert False, f"Отсутствует элемент с текстом: {text} в списке"
