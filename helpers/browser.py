from typing import Tuple
import allure
from selenium.webdriver.chrome.webdriver import ChromiumDriver
from .element import Element, Elements


class Browser:
    browser: ChromiumDriver

    def __init__(self, browser):
        self.browser: ChromiumDriver = browser

    def element(self, locator: Tuple[str, str]):
        return Element(self.browser, locator)

    def elements(self, locator: Tuple[str, str]):
        return Elements(self.browser, locator)

    def get_url(self):
        return self.browser.current_url

    def switch_to_window(self):
        window_handles = self.browser.window_handles
        if len(window_handles) == 1:
            raise WindowsError("Only one window available")
        self.browser.switch_to.window(window_handles[-1])

    def scroll_down(self, y):
        self.browser.execute_script(f"window.scrollTo({0}, {y});")

    def scroll_up(self, y):
        self.browser.execute_script(f"window.scrollTo({0}, {y * -1});")

    @allure.step("Проверка что объекты равны")
    def object_should_be_equal(self, first_obj, second_obj):
        assert first_obj == second_obj, f"{first_obj} != {second_obj}"
