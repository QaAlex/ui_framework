from base_test_suite import TestBaseSuite
from pages.ordermodal import OrderModal
from pages.orders_page import OrdersPage


class Test_modals(TestBaseSuite):
    def test_open_modals(self):
        order_page = self.get_page(OrdersPage)
        order_modal = order_page.click_on_order_card()
        order_modal.should_be_on_order_modals()

    def test_order_number_should_be_in_order_modal(self):
        order_page = self.get_page(OrdersPage)
        order_number = order_page.numbers_of_orders().texts[0]
        order_modal = order_page.click_on_order_card()
        order_modal.number_of_order().should_have_text(order_number)


    def test(self):
        order_page = self.get_page(OrderModal)
        order_page.number_of_order().click()

#
# После нажатия кнопки "Оформить заказ" и до сообщения "Заказ готов" , заказ находится с статусе "В работе"
# После завершения выполнения заказа заказ переходит с статус "Готов"
# Проверить работу счетчика заказов за день
# Проверить работу счетчика заказов за все время
# Номер в блоке "Лента заказов" соответсвует номеру заказа в модалке"
# Проверить время создания заказа
# Если номер заказа в списке "Готов", то в модалке написано , что заказ выполнен