from pages.main_page import MainPage
from pages.login_page import LoginPage
from conftest import driver
from data.register_data import main_account
from base_test_suite import TestBaseSuite


class TestLogin(TestBaseSuite):

    def test_login(self):
        main_page = self.get_page(MainPage)
        login_page = main_page.header_block.click_on_login_link()
        main_page = login_page.fill_login_form(main_account.email, main_account.password)
        main_page.should_be_main_page()

    def test_login_with_wrong_email(self):
        main_page = self.get_page(MainPage)
        login_page = main_page.header_block.click_on_login_link()
        login_page.fill_login_form(email="test12311@gmail.com", password="qwerty123", is_valid=False)
        login_page.error_message().should_have_text("Некорректный E-mail")

    def test_show_password(self):
        main_page = self.get_page(MainPage)
        login_page = main_page.header_block.click_on_login_link()
        login_page.fill_login_form(email="test12311@gmail.com", password="qwerty123", is_valid=False)
        login_page.click_on_show_password()
