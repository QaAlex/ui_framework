from base_test_suite import TestBaseSuite
from pages.main_page import MainPage
import allure
import pytest


class TestMainPage(TestBaseSuite):

    @allure.id("0001")
    @allure.title("Переход на страницу логина")
    def test_main_page_banner(self):
        main_page = self.get_page(MainPage)
        login_page = main_page.header_block.click_on_login_link()
        login_page.should_be_on_login_page()

    def test_go_to_personal_account_page(self, login):
        # TODO fix login
        main_page = login
        personal_account_page = main_page.header_block.click_on_login_link(is_resister=True)
        personal_account_page.should_be_personal_account_page()

    def test_hover(self):
        main_page = self.get_page(MainPage)
        main_page.enter_personal_account_btn().hover()

    def test_hide_element(self):
        main_page = self.get_page(MainPage)
        main_page.enter_personal_account_btn().hide()

    def test_switch_to_window(self):
        main_page = self.get_page(MainPage)
        main_page.header_block.orders_link().click_on_mouse_wheel()
        main_page.switch_to_window()

    def test_scroll_page(self):
        main_page = self.get_page(MainPage)
        main_page.scroll_down(100)
        main_page.scroll_up(100)

    def test_login_button_should_be_visible_for_not_auth_user(self):
        main_page = self.get_page(MainPage)
        main_page.enter_personal_account_btn().should_be_visible()
        main_page.enter_personal_account_btn().should_have_text("Войти в аккаунт")