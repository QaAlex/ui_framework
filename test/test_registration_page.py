from pages.main_page import MainPage
from pages.registration_page import RegistrationPage
from conftest import *
from pages.login_page import LoginPage
from data.register_data import register_data
from model.user_model import UserModel
from data.register_data import main_account
from base_test_suite import TestBaseSuite


@pytest.fixture
def user_data() -> UserModel:
    return register_data


class TestRegister(TestBaseSuite):
    def test_registration(self, user_data):
        main_page = self.get_page(MainPage)
        login_page = main_page.header_block.click_on_login_link()
        register_page = login_page.click_on_registration_link()
        register_page.should_be_on_registration_page()
        login_page = register_page.fill_registration_form(register_data.name, register_data.email,
                                                          register_data.password)
        login_page.should_be_on_login_page()
        login_page.fill_login_form(register_data.email, register_data.password)
        main_page.should_be_main_page()

    def test_registration_message_user_already_exists(self):
        register_page = self.get_page(RegistrationPage) #RegistrationPage(driver).open_url()
        register_page.should_be_on_registration_page()
        register_page.fill_registration_form("Alan", main_account.email, main_account.password, is_valid=False)
        message = register_page.get_message_user_already_exists()
        assert message == 'Такой пользователь уже существует'

    def test_wrong_password(self):
        register_page = self.get_page(RegistrationPage)#RegistrationPage(driver).open_url()
        register_page.should_be_on_registration_page()
        register_page.fill_registration_form(register_data.name, register_data.email, "1")
