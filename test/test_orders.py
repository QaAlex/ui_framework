import allure

from base_test_suite import TestBaseSuite
from pages.orders_page import OrdersPage


class TestOrders(TestBaseSuite):

    @allure.title("Получить список заказов")
    @allure.feature("Работа с заказами")
    @allure.epic("Bla bla bla")
    def test_get_order_list(self):
        order_page = self.get_page(OrdersPage)
        order_list = order_page.order_list()
        with allure.step("Проверка количества заказов"):
            order_page.object_should_be_equal(len(order_list), 5)
        assert '016422' in order_list.texts

    @allure.title("Открыть модалку заказа")
    def test_click_on_order_card(self):
        order_page = self.get_page(OrdersPage)
        order_card = order_page.order_card()
        order_card.click()
