from model.user_model import UserModel
from pages.main_page import MainPage
import pytest

from base_test_suite import TestBaseSuite


class TestPersonalAccount(TestBaseSuite):
    # @pytest.mark.xfail
    def test_change_user_data(self):
        main_page = self.login()
        personal_account_page = main_page.header_block.click_on_login_link(is_resister=True)
        personal_account_page.change_user_data(name='Test Name')


    def test_exif_personal_account(self):
        main_page = self.login()
        personal_account_page = main_page.header_block.click_on_login_link(is_resister=True)
        personal_account_page.exif_personal_account()