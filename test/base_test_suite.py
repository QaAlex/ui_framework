import allure
import pytest

from model.user_model import UserModel
from pages.base_page import BasePage
from selenium.webdriver.chromium.webdriver import ChromiumDriver
import typing

from pages.main_page import MainPage

T = typing.TypeVar("T", bound=BasePage)


class TestBaseSuite:
    browser: ChromiumDriver

    @pytest.fixture(autouse=True)
    def prepare(self, driver):
        self.browser: ChromiumDriver = driver

    @allure.step("Открыть страницу: {page_class}")
    def get_page(self, page_class: typing.Type[T]) -> T:
        page = page_class(self.browser)
        page.open()
        return page


    @allure.step("Авторизация")
    def login(self):
        user = UserModel(name=None, email="test123@gmail.com", password="qwerty123")
        main_page = self.get_page(MainPage)
        login_page = main_page.header_block.click_on_login_link()
        login_page.fill_login_form(user.email, user.password)
        return MainPage(self.browser)

